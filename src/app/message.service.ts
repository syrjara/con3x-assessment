import { Injectable } from '@angular/core';
import Web3 from 'web3';
import MessageContract from '../assets/messageContract.json';

declare let window: any;

@Injectable({
  providedIn: 'root',
})
export class MessageService {
  private web3: any;
  private contract: any;
  private contractAddress = '0xab90187d43a5c9368ac9c739541c3a898213a55d';

  constructor() {
    if (typeof window.ethereum !== 'undefined') {
      this.web3 = new Web3(window.ethereum);
      this.contract = new this.web3.eth.Contract(MessageContract,this.contractAddress);
    } else {
      console.error('install metamask');
    }
  }

  async getMessage(): Promise<string> {
    return await this.contract.methods.readMessage().call();
  }
  async setMessage(message: string): Promise<void> {
    const accounts = await this.web3.eth.getAccounts();
    console.log(accounts);
    const fromAddress = accounts[0];
    if ((accounts.length = 0)) {
      try {
        window.ethereum.request({ method: 'eth_requestAccounts' });
      } catch (error) {
        return;
      }
    }
    await this.contract.methods.saveMessage(message).send({ from: fromAddress });
  }
}
