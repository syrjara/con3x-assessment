import { Component, OnInit } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { RouterOutlet } from '@angular/router';
import { MessageService } from './message.service';
@Component({
  selector: 'app-root',
  standalone: true,
  imports: [FormsModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
  providers:[MessageService]
})
export class AppComponent implements OnInit {
  title:string='cod3x'
  message: string = '';
  newMessage: string = '';

  constructor(private messageService: MessageService) { }

  async ngOnInit() {
    this.message = await this.messageService.getMessage();
  }

  async setMessage() {
    await this.messageService.setMessage(this.newMessage);
    this.message = await this.messageService.getMessage();
    this.newMessage = '';
  }
}