// SPDX-License-Identifier: MIT
pragma solidity ^0.8.24;

contract Con3xContract {
    string private message;
    function saveMessage(string memory _message) public {
        message = _message;
    }
    function readMessage() public view returns (string memory) {
        return message;
    }
}
